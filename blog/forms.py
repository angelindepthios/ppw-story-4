from django.forms import ModelForm
from .models import Post, Year

YEAR_CHOICE= (("2019","2019"),("2018","2018"),("2017","2017"),("2016","2016"),)
class FriendsForm(ModelForm):
    class Meta:
        model = Post
        fields = '__all__'
class Year(ModelForm):
    class Meta:
        model= Year
        fields = '__all__'  