from django.db import models
class Year(models.Model):
    YEARS_CHOICE = (("2016","2016"),("2017","2017"),("2018","2018"),("2019","2019"))
    year_choice = models.TextField(
    choices= YEARS_CHOICE,null=False)
    def __str__(self):
        return "{}".format(self.year_choice)
class Post(models.Model):
    FirstName = models.CharField(null=True, max_length = 30)
    LastName= models.CharField(null=True, max_length = 30)
    FavouriteFood= models.CharField(null=True, max_length = 30)
    FavouriteDrink= models.CharField(null=True, max_length = 30)
    Hobby = models.CharField(null=True, max_length = 30)
    YEARS_CHOICE = (("2016","2016"),("2017","2017"),("2018","2018"),("2019","2019"))
    year_choice = models.ForeignKey(Year,on_delete=models.CASCADE,null=False)
    def __str__(self):
        return "{}".format(self.FirstName)
