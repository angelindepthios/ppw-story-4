from django.shortcuts import render
from blog.forms import FriendsForm
from blog.models import Post, Year
def home(request):
    context = {
        'profile':'hi1.png','home':'home.jpg'
            }
    return render(request,'home.html',context)
def about(request):
    context ={'back':'backk2.jpg','profile':'hi1.png','profileid':'profile.png','ID':'id.png','university':'university.png','univ':'uni.png','edu':'education.png','exp':'experience.png','hobby':'hobby.png'
    }
    return render(request,'about1.html',context)
def description(request):
    context= {'middle':'middle1.jpg','one':'1.jpg','me':'mee.jpg'}
    return render(request,'description.html',context)
def socialmedia(request):
    context = { 'ig':'instagram.png','line':'line.png','mail':'mail.png','music':'music.png'}  
    
    return render(request,'socialmedia2.html',context)
def formpage(request):
    return render(request,'form.html')

def FormPage(request):
    form =FriendsForm(request.POST)
    friends= Post.objects.all()
    #friends= Post.objects.filter(year_choice = Year.objects.filter(year_choice = 2016)[0])
    #print(friends)
    if request.method=="POST":
        if form.is_valid():
            form.save()
            return render(request,'display.html',{'friendss':friends})
    else :
        form = FriendsForm()
    return render(request,'form.html',{'form':form})